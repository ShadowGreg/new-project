﻿using Microsoft.AspNetCore.Connections;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using Microsoft.Extensions.Hosting;
using System.Text;

namespace WebMyClient.RabbitMQ
{
    public class RabbitMqListener : BackgroundService
    {
        private IConnection _connection;
        private IModel _channel;
        private IModel _channel1;

        public RabbitMqListener()
        {
            // Не забудьте вынести значения "localhost" и "MyQueue"
            // в файл конфигурации
            //var factory = new ConnectionFactory { HostName = "localhost" }; // for local docker container
            var factory = new ConnectionFactory() { Uri = new Uri("amqps://eexypedf:USefAchiBrLP35STdTQJ8ziZ1Qr0L86A@sparrow.rmq.cloudamqp.com/eexypedf") };
            _connection = factory.CreateConnection();

            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: "TesterToAdminQueue", durable: false, exclusive: false, autoDelete: false, arguments: null);

            _channel1 = _connection.CreateModel();
            _channel1.QueueDeclare(queue: "HomeworkToAdminQueue", durable: false, exclusive: false, autoDelete: false, arguments: null);

        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());

                Console.WriteLine("Receive message consumer: " + content);

                _channel.BasicAck(ea.DeliveryTag, false);
            };

            _channel.BasicConsume("TesterToAdminQueue", false, consumer);

            var consumer1 = new EventingBasicConsumer(_channel1);
            consumer1.Received += (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());

                Console.WriteLine("Receive message consumer1: " + content);

                _channel1.BasicAck(ea.DeliveryTag, false);
            };

            _channel1.BasicConsume("HomeworkToAdminQueue", false, consumer1);

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }
    }
}
