﻿using AutorizationMcsContract;
using ShluzAPIswagger.Models;

namespace ShluzAPIswagger.Services.Abstraction
{
    public interface IUserService
    {
        public User Get(UserLogin userLogin);
        public int Set(UserAutorizationModel user);

        public int GiveUserRole(int userId, string role);
    }
}
