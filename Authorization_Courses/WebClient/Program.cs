﻿using System.Numerics;
using System.Text.Json;
using System;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Security.Policy;
using Castle.Core.Resource;
using System.Net.Http.Json;

namespace WebClient
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            using (var client = new HttpClient())
            {
                string url = "https://localhost:7029/api/User";
                string json;
                string password;

                Console.WriteLine("Please enter Register[R]/Asiign[A]:");
                string choose = Console.ReadLine();

                if (choose == "R")
                {
                    Console.WriteLine("Please enter First Name:");
                    string fName = Console.ReadLine();

                    Console.WriteLine("Please enter Last Name:");
                    string lName = Console.ReadLine();

                    Console.WriteLine("Please enter email:");
                    string email = Console.ReadLine();

                    Console.WriteLine("Please enter password:");
                    password = Console.ReadLine();

                    UserModel uModel = RegistrationLearnPlatform(fName, lName, email, password);

                    json = JsonSerializer.Serialize(uModel);

                    Console.WriteLine("Send request ....");

                    try
                    {
                        using (HttpContent cont = new StringContent(json, Encoding.UTF8, "application/json"))
                        {
                            var response = await client.PostAsync(url, cont);
                            var result = await response.Content.ReadAsStringAsync();

                            Console.WriteLine("Add object User with Id = " + result);

                            // Take transform UserModel object from json request 
                            var task = await client.GetFromJsonAsync("https://localhost:7029/api/User/" + result, typeof(UserModel));
                            UserModel usMy = (UserModel)task;

                            // Take string exist our new User
                            await getCustomerInfoFromServer(client, result);

                            Console.ReadLine();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Bad request!");
                    }
                }
                else
                {
                    Console.WriteLine("Please enter email:");
                    string email = Console.ReadLine();

                    Console.WriteLine("Please enter password:");
                    password = Console.ReadLine();

                    json = JsonSerializer.Serialize(email);

                    Console.WriteLine("Send assign request ....");

                    try
                    {

                        // Take UserModel object from json request 
                        //var task = await client.GetFromJsonAsync("https://localhost:7029/api/User/" + email, typeof(UserModel));        
                        //UserModel usMy = (UserModel)task;

                        // Take user id by assign email and password
                        Console.WriteLine("Assign User Id:");
                        Console.WriteLine(await client.GetStringAsync("https://localhost:7029/api/User/" + email + "/" + password));


                        // Совпадение пароля проверяется на стороне автризатора !!
                        //bool logIn = LoginLearnPlatform(password, usMy.Hash, usMy.Salt);

                        //if (logIn)
                        //    Console.WriteLine("User Assigned in Learn Platform");
                        //else
                        //    Console.WriteLine("User denied!");

                        Console.ReadLine();

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Bad request!");
                    }
                }
            }

            static bool LoginLearnPlatform(string password, byte[] myHash, byte[] mySalt)
            {

                bool myVerify = VerifyPassword(password, myHash, mySalt);

                return myVerify;
            }

            static UserModel RegistrationLearnPlatform(string fName, string lName, string email, string password)
            {
                UserModel userModel = new UserModel();

                userModel.FirstName = fName;
                userModel.LastName = lName;
                userModel.Email = email;
                userModel.CreateDate = DateTime.Now;
                userModel.UpDate = DateTime.Now;
                byte[] mySalt;
                userModel.Hash = HashPassword(password, out mySalt);
                userModel.Salt = mySalt;
                userModel.Deleted = false;

                return userModel;
            }

            static async Task getCustomerInfoFromServer(HttpClient client, string id)
            {
                Console.WriteLine("User Info:");
                Console.WriteLine(await client.GetStringAsync("https://localhost:7029/api/User/" + id));
            }


            static byte[] HashPassword(string password, out byte[] salt)
            {
                const int keySize = 20;
                const int iterations = 350000;
                HashAlgorithmName hashAlgorithm = HashAlgorithmName.SHA512;

                salt = RandomNumberGenerator.GetBytes(keySize);

                var hash = Rfc2898DeriveBytes.Pbkdf2(
                    Encoding.UTF8.GetBytes(password),
                    salt,
                    iterations,
                    hashAlgorithm,
                    keySize
                    );

                return hash;
            }

            static bool VerifyPassword(string password, byte[] hash, byte[] salt)
            {
                const int keySize = 20;
                const int iterations = 350000;
                HashAlgorithmName hashAlgorithm = HashAlgorithmName.SHA512;

                var hashToCompare = Rfc2898DeriveBytes.Pbkdf2(password, salt, iterations, hashAlgorithm, keySize);

                return hashToCompare.SequenceEqual(hash);
            }
        }
    }

    internal class UserModel
    {
        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public string? Email { get; set; }

        public byte[] Salt { get; set; } = new byte[20];


        public byte[] Hash { get; set; } = new byte[20];


        public DateTime CreateDate { get; set; }

        public DateTime UpDate { get; set; }

        public bool Deleted { get; set; }
    }
}