﻿namespace ShluzAPIswagger.Models
{
    public class Promotion
    {
        public string Id { get; set; }
        public string Title { get; set; } = null!;
        //public string Description { get; set; } = null!;
        public string Promo { get; set; } = null!;
        public List<Lesson> Lessons { get; set; } = null!;
    }
    public class Lesson
    {
        public string Title { get; set; } = null!;
        //public string Description { get; set; } = null!;
    }

}
