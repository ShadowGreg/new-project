﻿using MongoDB.Driver;
using СourseСatalog.Domain;
using СourseСatalog.Infrastructure.Repositories.Implementations;

namespace СourseСatalog.Services
{
    public class PromotionServiceCached : IPromotionsService
    {
        private readonly IPromotionsService _promotionService;
        private List<Promotion> Promotions { get; set; }
        public PromotionServiceCached(IPromotionsService promotionsService) {
            _promotionService = promotionsService;
        }

        public async Task<List<Promotion>> GetPromotions()
        {
            Promotions ??= await _promotionService.GetPromotions();

            return Promotions;
        }

        public async Task Update()
        {
            Promotions = await _promotionService.GetPromotions();
        }
    }
}
