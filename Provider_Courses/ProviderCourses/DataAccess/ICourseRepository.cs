﻿namespace ProviderCourses.DataAccess
{
    public interface ICourseRepository
    {
     
        Task<List<CourseDTO>> GetAsync();
        Task<CourseDTO> GetAsync(string id);
       
    }


}
