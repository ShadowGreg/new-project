﻿using CourseEditor.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
//using SharedMongoDBModelLibrary;
using CourseEditorContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using AutoMapper;
using CourseEditor.DataAccess;
using System.Diagnostics.CodeAnalysis;

namespace LessonEditor.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class LessonController : ControllerBase
    {
   
        private readonly ILogger<LessonController> _logger;
        private readonly ILessonRepository _repo;
        private readonly IMapper _mapper;


        public LessonController(ILogger<LessonController> logger, ILessonRepository repo,IMapper mapper)
        {
            _logger = logger;
            _repo = repo;
            _mapper = mapper;
        }

        /// <summary>
        /// Добавить урок
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="lesson"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<DBResponse> Add([FromHeader] string courseId, [FromHeader] string Title , [FromHeader] string Description, [FromHeader][AllowNull] string? Homework , [FromHeader][AllowNull]  string? Links ,[FromBody,AllowNull]  List<Question>? Test )
        {
            if (Test is null) Test = new List<Question>();
            return await  _repo.NewLesson(courseId,Title,Description,Homework,Links,Test);
        }

        [HttpDelete]
        public async Task<DBResponse> Delete([FromHeader] string courseId, [FromHeader] string id)
        {
            return await _repo.DelLesson(courseId, id );
        }

        /// <summary>
        /// Редактировать урок
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="lesson"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<DBResponse> Update([FromHeader] string courseId, [FromBody] CourseEditorContract.Lesson lesson)
        {
            return await _repo.EditLesson(courseId,_mapper.Map<SharedMongoDBModelLibrary.Lesson>(lesson));
        }

        /// <summary>
        /// Поменять порядок уроков
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="id"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<DBResponse> Order([FromHeader] string courseId, [FromHeader] string id, [FromHeader] int order)
        {
            return await _repo.ChangeOrder(courseId, id, order);
        }

        /// <summary>
        /// Список уроков курса
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<CourseEditorContract.Lesson> GetAll([FromHeader] string courseId)
        {
            return _mapper.Map <IEnumerable <CourseEditorContract.Lesson >>( _repo.Get(courseId));
        }

        /// <summary>
        /// Посмотреть урок
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public CourseEditorContract.Lesson Get([FromHeader]  string courseId, [FromHeader]  string id)
        {
            return _mapper.Map<CourseEditorContract.Lesson>(_repo.Get( courseId,id));
        }




    }
}
