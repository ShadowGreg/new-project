﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseProviderContract
{
    public class Response
    {
        public string ?Text { get; set; } // text of response to question
        public bool IsTrue { get; set; } // right/wrong // т.е. является ли он верным ответом ???
    }
}
