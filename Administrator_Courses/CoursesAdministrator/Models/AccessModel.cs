﻿namespace CoursesAdministrator.Models
{
    public class AccessModel
    {
        public long UserId { get; set; }
        public string CourseId { get; set; }
        public string LessonId { get; set; }

    }
}
