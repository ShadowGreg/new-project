﻿using Microsoft.Extensions.Hosting;
using CourseEditor.DataAccess;

namespace CourseEditor.Services
{
    public  class Worker : BackgroundService, IWorker
    {

        private readonly ICourseRepository _courseRepository;

        public Worker(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (true)
            {
                var courses = await _courseRepository.GetAsync();
                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}