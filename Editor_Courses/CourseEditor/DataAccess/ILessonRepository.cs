﻿using CourseEditor.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedMongoDBModelLibrary;

namespace CourseEditor.DataAccess
{
    public interface ILessonRepository
    {
        Task<DBResponse> NewLesson(string courseId, string title, string description, string homework, string links, List<CourseEditorContract.Question> test);
        List<Lesson> Get(string courseId);
        Lesson Get(string courseId,string id);
        Task<DBResponse> DelLesson(string courseId, string id);
        Task<DBResponse> EditLesson(string courseId, Lesson lesson);
        Task<DBResponse> ChangeOrder(string courseId, string id, int order);

    }

}
