﻿using AutoMapper;

namespace Homework.Mapping
{
    public class MessagesChat:Profile
    {
        public MessagesChat()
        {
            CreateMap< Entity.Message,string>().ConvertUsing(src => src.CreateDate + "|" + src.Text);
               
        }
    }
}
