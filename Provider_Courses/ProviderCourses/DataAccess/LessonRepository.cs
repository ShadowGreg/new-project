﻿using ProviderCourses.DataAccess;
using Microsoft.Extensions.Options;
using MongoDB.Driver;


namespace ProviderCourses.DataAccess
{
    public class LessonRepository : ILessonRepository
    {
        private readonly IMongoCollection<CourseDTO> _coursesCollection;
        public LessonRepository(IOptions<CourseStoreDatabaseSettings> courceStoreDatabaseSettings, IDBConnect con, IDBMongoConnectGet con2)
        {
            con.Connect(courceStoreDatabaseSettings);
            _coursesCollection = con2.GetConnect();
        }
     
        public LessonDTO Get(string courseId, string id) =>
             _coursesCollection.Find(x => x.Id == courseId).First().Lessons.Find(l => l.Id == id);

        public List<LessonDTO> Get(string courseId) =>
             _coursesCollection.Find(x => x.Id == courseId).First().Lessons;
       
    }
}
