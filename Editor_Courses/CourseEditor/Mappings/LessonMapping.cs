﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseEditor.Mappings
{
    public  class LessonMapping : Profile
    {
        public LessonMapping()
        {
            CreateMap<SharedMongoDBModelLibrary.Lesson, CourseEditorContract.Lesson>().ReverseMap()
                .ForMember(dest => dest.DateUpdated, opt => opt.MapFrom(src => DateTime.Now))
                ;
        }
    }
}
