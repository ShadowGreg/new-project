﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Homework.Entity
{
    public class Message
    {
        [Key]
        public long Id { get; set; }
        public long UserId { get; set; }
        public string? Text { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpDate { get; set; }
        public bool Deleted { get; set; } // Deleted // 1 - deleted, 0 - not deleted

        // Связь с таблицей Homework
        public long HomeworkId { get; set; }
        [ForeignKey("HomeworkId")]
        [InverseProperty("Messages")]
        public virtual Homework Homework { get; set; }
    }
}
