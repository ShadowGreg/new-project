﻿namespace Homework.Services
{
    public interface IRabbit
    {
        void SendMessage(object obj, string severity);
    }
}
