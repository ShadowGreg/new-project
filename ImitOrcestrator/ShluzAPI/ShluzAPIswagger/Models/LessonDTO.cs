﻿using Microsoft.AspNetCore.Mvc;
using SharedMongoDBModelLibrary;
using System.Diagnostics.CodeAnalysis;

namespace ShluzAPIswagger.Models
{
    public class LessonDTO
    {
        public string courseId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string? Homework { get; set; }
        public string? Links { get; set; }  // <<<<<----- ошибка должен быть списком/массивом!!??
        public List<Question>? Test { get; set; }
    }
}
