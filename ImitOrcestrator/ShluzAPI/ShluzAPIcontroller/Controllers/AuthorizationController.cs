﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using ShluzAPIcontroller.Models;
using ShluzAPIcontroller.Services.Abstraction;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ShluzAPIcontroller.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        IUserService _userService;
        WebApplicationBuilder _builder;

        public AuthorizationController(IUserService userService, WebApplicationBuilder builder)
        { 
            _userService = userService;
            _builder = builder;
        }

        // GET: api/<AuthorizationController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<AuthorizationController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        /// <summary>
        /// [Вход юзера на платформу, с указанием email и пароля]
        /// </summary>
        // POST api/<AuthorizationController>
        [HttpPost("/login")]
        public IResult Post([FromBody] UserLogin user)
        {
            if (!string.IsNullOrEmpty(user.Username) &&
                    !string.IsNullOrEmpty(user.Password))
            {
                var loggedInUser = _userService.Get(user);
                if (loggedInUser is null) 
                    return Results.NotFound("User not found");

                var claims = new[]
                {
                        new Claim(ClaimTypes.Email, loggedInUser.Email),
                        new Claim(ClaimTypes.GivenName, loggedInUser.FirstName),
                        new Claim(ClaimTypes.Surname, loggedInUser.LastName),
                        new Claim(ClaimTypes.Role, loggedInUser.Role)
                    };

                var token = new JwtSecurityToken
                (
                    issuer: _builder.Configuration["Jwt:Issuer"],
                    audience: _builder.Configuration["Jwt:Audience"],
                    claims: claims,
                    expires: DateTime.UtcNow.AddDays(60),
                    notBefore: DateTime.UtcNow,
                    signingCredentials: new SigningCredentials(
                        new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_builder.Configuration["Jwt:Key"])),
                        SecurityAlgorithms.HmacSha256)
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

                return Results.Ok(tokenString);
            }
            return Results.BadRequest("Invalid user credentials");
        }

        // PUT api/<AuthorizationController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<AuthorizationController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
