using CoursesAdministrator.Infrastructure;
using CoursesAdministrator.Services;
using CoursesAdministrator.Infrastructure.EntityFramework;
using CoursesAdministrator.Infrastructure.Repositories.Implementations;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Builder;
using CoursesAdministrator.RabbitMQ;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddRazorPages();
builder.Services.AddOptions();
builder.Configuration.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
builder.Services.Configure<LearnPlatformDatabaseSettings>(
    builder.Configuration.GetSection("ConnectionStrings"));
builder.Services.Configure<RabbitMQSettings>(
    builder.Configuration.GetSection("RabbitMQSettings"));

builder.Services.AddDbContext<LearnPlatformContext>(o => o.UseNpgsql(builder.Configuration.GetConnectionString("db")));

builder.Services.AddScoped(typeof(DbContext), typeof(LearnPlatformContext));
builder.Services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
builder.Services.AddScoped<IAccessService, AccessService>();
builder.Services.AddScoped<IProgressService, ProgressService>();
builder.Services.AddScoped<IAuthorizerConnector, AuthorizerConnector>();
builder.Services.AddScoped<IRabbitMqService, RabbitMqService>();
builder.Services.AddHostedService<RabbitMqListener>();


builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Course administartor API",
        Description = "������������� ������. ������/�������� ������� ������, ������ ��������� �� ������/��.",
    });
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
        options.RoutePrefix = string.Empty;
    });
}
app.MapControllers();
app.Run("https://localhost:7239");
