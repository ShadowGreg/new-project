﻿using CourseEditorContract;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseEditor.Pattern
{
    public class LessonBuilder
    {
        private SharedMongoDBModelLibrary.Lesson _lesson;

        public LessonBuilder(string title, string description)
        {
            _lesson = new SharedMongoDBModelLibrary.Lesson
            {
                Id= ObjectId.GenerateNewId().ToString(),
                Title=title,
                Description=description,                
            };
        }

        public LessonBuilder AddTest()
        {
            _lesson.Test = new List<SharedMongoDBModelLibrary.Question>();
            return this;
        }

        public LessonBuilder AddTestQuestion(SharedMongoDBModelLibrary.Question q)
        {
            _lesson.Test.Add(q);
            return this;
        }

        public LessonBuilder AddHomework(string homework)
        {
            _lesson.Homework = homework;
            return this;
        }

        public LessonBuilder AddLinks(string links)
        {
            _lesson.Links = links.Split(",").ToList();
            return this;
        }
        public SharedMongoDBModelLibrary.Lesson Build()
        {
            _lesson.DateCreated = DateTime.Now;
            _lesson.IsRemoved = false;
            return _lesson;
        }
    }
}
