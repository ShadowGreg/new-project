﻿using CourseEditorContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseEditor.Pattern
{
    public class TestIterator : IIterator<SharedMongoDBModelLibrary.Question>
    {
        private List<CourseEditorContract.Question> _test;
        private int _current;

        public TestIterator(List<CourseEditorContract.Question> test)
        {
            _test = test;
            _current = 0;
        }

        public SharedMongoDBModelLibrary.Question GetNext()
        {
            var c = _test.ElementAt(_current);
            _current++;
            var q = MapResponces(c).ToList();
            return new SharedMongoDBModelLibrary.Question { Responses = q,Text=c.Text};
            
        }
        IEnumerable<SharedMongoDBModelLibrary.Response> MapResponces(CourseEditorContract.Question quest)
        {
            foreach (Response r in quest.Responses)
            {
                yield return new SharedMongoDBModelLibrary.Response
                {
                    Text = r.Text,
                    IsTrue = r.IsTrue
                };
            }
        }

        public bool HasNext()
        {
            return _test.Count == _current+1;
        }

        public void Reset() => _current = 0;
    }
}
