﻿using СourseСatalog.Domain;

namespace СourseСatalog.Services
{
    public interface IPromotionsService
    {
        Task<List<Promotion>> GetPromotions();
    }
}
