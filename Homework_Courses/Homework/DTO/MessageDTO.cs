﻿namespace Homework.DTO
{
    public class MessageDTO
    {

        //public long Id { get; set; }
        public string Text { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpDate { get; set; }
        //public bool Deleted { get; set; } // Deleted // 1 - deleted, 0 - not deleted
        public long UserId { get; set; }

        // Связь с таблицей Homework
        public long HomeworkId { get; set; }
    }
}
