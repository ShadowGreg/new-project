﻿using RabbitMQ.Client;
using System.Text.Json;
using System.Text;

namespace WebAPIClient.RabbitMQ
{
    public class RabbitMqSender : IRabbitMqService
    {
        public void SendMessage(object obj, string severity)
        {
            var message = JsonSerializer.Serialize(obj);
            SendMessage(message, severity);
        }

        public void SendMessage(string message, string severity)
        {
            // Не забудьте вынести значения "localhost" и "MyQueue"
            // в файл конфигурации
            //var factory = new ConnectionFactory() { HostName = "localhost" }; // for local docker container
            var factory = new ConnectionFactory() { Uri = new Uri("amqps://eexypedf:USefAchiBrLP35STdTQJ8ziZ1Qr0L86A@sparrow.rmq.cloudamqp.com/eexypedf") };

            //string direct = "direct_admin";
            string direct = "direct_homework";

            using (var connection = factory.CreateConnection())
            
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: direct, type: ExchangeType.Direct);

                //var severity = "tester";

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: direct,
                     routingKey: severity,
                     basicProperties: null,
                     body: body);

                Console.WriteLine($" [x] Sent '{severity}':'{message}'");

                //channel.QueueDeclare(queue: "MyQueueRvrs",
                //    durable: false,
                //    exclusive: false,
                //    autoDelete: false,
                //    arguments: null);

                //var body = Encoding.UTF8.GetBytes(message);

                //channel.BasicPublish(exchange: "",
                //    routingKey: "MyQueueRvrs",
                //    basicProperties: null,
                //    body: body);
            }
        }
    }
}
