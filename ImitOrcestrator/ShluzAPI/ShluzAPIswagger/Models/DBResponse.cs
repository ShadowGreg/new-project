﻿namespace Learn_Platform_shluz.Models
{
    public class DBResponse
    {
        public string? Id { get; set; }
        public string? ResultText { get; set; }
        public string? Code { get; set; }

    }
}
