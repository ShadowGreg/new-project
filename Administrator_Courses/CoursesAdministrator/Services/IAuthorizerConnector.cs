﻿namespace CoursesAdministrator.Services
{
    public interface IAuthorizerConnector
    {
        Task<List<string>> GetRole(long userId);
    }
}
