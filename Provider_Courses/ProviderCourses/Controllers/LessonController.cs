﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProviderCourses.DataAccess;

namespace ProviderCourses.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LessonController : ControllerBase
    {

        private readonly ILogger<LessonController> _logger;
        private readonly ILessonRepository _repo;


        public LessonController(ILogger<LessonController> logger, ILessonRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        [HttpGet("{courseId}")]
        public IEnumerable<LessonDTO> Get(string courseId) => (IEnumerable<LessonDTO>)_repo.Get(courseId);
    
        [HttpGet("{courseId}/{id}")]
        public LessonDTO Get(string courseId, string id) => _repo.Get(courseId, id);

    }
}
