﻿namespace CoursesAdministrator.Infrastructure
{
    public class LearnPlatformDatabaseSettings
    {
        public string ConnectionString { get; set; } = null!;
    }
}
