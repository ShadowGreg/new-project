﻿namespace CoursesAdministrator.RabbitMQ
{
    public interface IRabbitMqService
    {
        void SendMessage(object obj, string severity);
    }
}
