﻿using NCrontab;
using СourseСatalog.Services;

namespace СourseСatalog.Background
{
    public class PromotionUpdateService : BackgroundService
    {
        private string Schedule => "0 0 * * * *";
        private readonly CrontabSchedule _schedule;
        private DateTime _nextRun;
        private PromotionServiceCached _promotionServiceCached;

        public PromotionUpdateService(PromotionServiceCached promotionServiceCached)
        {
            _schedule = CrontabSchedule.Parse(Schedule, new CrontabSchedule.ParseOptions { IncludingSeconds = true });
            _nextRun = _schedule.GetNextOccurrence(DateTime.Now);
            _promotionServiceCached= promotionServiceCached;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var now = DateTime.Now;

                if (now > _nextRun)
                {
                    _nextRun = _schedule.GetNextOccurrence(now);
                    await _promotionServiceCached.Update();
                }

                await Task.Delay(30000, stoppingToken);
            }
        }
    }
}
