﻿using ShluzAPIswagger.Models;
using SharedMongoDBModelLibrary;
using ShluzAPIswagger.Services.Abstraction;
using Learn_Platform_shluz.Models;

namespace ShluzAPIswagger.Services.Implementation
{
    public class CourseProviderService : ICourseProviderService
    {
        public CourseProviderDTO Get(string courseId)
        {
            try
            {
                return GetCourseByIdAsync(courseId).Result;
            } catch (Exception ex)
            { 
                Console.WriteLine(ex.Message);
                return null;
            }
            
        }

        public List<Course> GetAll()
        {
            try
            {
                return GetCoursesAsync().Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        async Task<List<Course>> GetCoursesAsync()
        {
            string url = "https://localhost:7078/api/Course"; // 

            List<Course> courses = null;

            using (var client = new HttpClient())
            {
                //  
                var task = await client.GetFromJsonAsync(url, typeof(List<Course>));
                courses = (List<Course>)task;
            }

            return courses;
        }

        async Task<CourseProviderDTO> GetCourseByIdAsync(string courseId)
        {
            string url = "https://localhost:7078/api/Course/" + courseId; // 

            CourseProviderDTO course = null;

            using (var client = new HttpClient())
            {
                //  
                var task = await client.GetFromJsonAsync(url, typeof(CourseProviderDTO));
                course = (CourseProviderDTO)task;
            }

            return course;
        }
    }
}
