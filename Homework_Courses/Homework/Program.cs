using Homework.Config;
using Homework.DataAccess;
using Homework.Mapping;
using Homework.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.OpenApi.Models;
using Npgsql;

namespace Homework
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Configuration.AddJsonFile("appsettings.json");

            builder.Services.AddHealthChecks().AddDbContextCheck<NpgContext>();

            builder.Services.AddControllers();
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "My API - V1",
                        Version = "v1"
                    }
                 );

                var filePath = Path.Combine(System.AppContext.BaseDirectory, "MyApi.xml");
                c.IncludeXmlComments(filePath);
            });

            builder.Services.AddDbContext<NpgContext>(x => x.UseNpgsql(builder.Configuration.GetConnectionString("homework")));

            builder.Services.AddTransient<IHomeworkRepo, HomeworkRepo>();

            builder.Services.AddTransient<IRabbit, RabbitService>();

            builder.Services.AddAutoMapper(typeof(HomeworkMap), typeof(MessageMap), typeof(MessagesChat));

            builder.Services.AddOptions<AppConfig>().Configure(opt => {
                opt.DBconnection = builder.Configuration.GetConnectionString("homework");
                opt.MQconnection = builder.Configuration.GetConnectionString("rabbit");
                opt.MQqueue = builder.Configuration.GetValue<string>("MQqueue");
                });

            var app = builder.Build();

            
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(e => e.MapHealthChecks("/health"));


            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.MapControllers();

            app.Run("http://localhost:5206");
        }
    }
}