﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Services.Abstractions;
using Microsoft.Extensions.Options;
using System.Diagnostics;
using System.Text.Json;
using PostgreSQLModelsLibrary;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Channels;

namespace Services.Implementations
{
    public class RabbitMqListener : BackgroundService
    {
        private IConnection _connection;
        private IModel _channel;
        private readonly IRabbitMqOptions _options;

        public RabbitMqListener(IServiceProvider _serviceProvider)
        {
            using var scope = _serviceProvider.CreateScope();
            var options = scope.ServiceProvider.GetRequiredService<IOptions<RabbitMqOptions>>();

            _options = options.Value;

            ConnectionFactory factory;

            if (options.Value.Host == "localhost")
            {
                // Не забудьте вынести значения "localhost" и "MyQueue"
                // в файл конфигурации
                factory = new ConnectionFactory() { HostName = _options.Host }; // for local docker container
            }
            else 
            {
                factory = new ConnectionFactory() { Uri = new Uri(_options.Host) };
            }


            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            //_channel.QueueDeclare(queue: _options.QueueNameFrom, durable: false, exclusive: false, autoDelete: false, arguments: null);

            /*--------------------------------*/
            _channel.ExchangeDeclare(exchange: "direct_admin", type: ExchangeType.Direct);
            /*--------------------------------*/
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            /*--------------------------------*/

            // declare a server-named queue
            var queueName = _channel.QueueDeclare().QueueName;

            _channel.QueueBind(queue: queueName,
                      exchange: "direct_admin",
                      routingKey: "tester"); // severity <<<<<--------

            Console.WriteLine(" [*] Waiting for messages.");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                var routingKey = ea.RoutingKey;
                Console.WriteLine($" [x] Received '{routingKey}':'{message}'");
            };
            _channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);

            /*--------------------------------*/

            //var consumer = new EventingBasicConsumer(_channel);
            //consumer.Received += (ch, ea) =>
            //{
            //    var progressObj = Encoding.UTF8.GetString(ea.Body.ToArray());

            //    var content = JsonSerializer.Deserialize<Progress>(progressObj);    //Encoding.UTF8.GetString(ea.Body.ToArray());       

            //    Console.WriteLine("Receive message Progress: " + progressObj);

            //    // Send to user email integral result of test
            //    // ....

            //    _channel.BasicAck(ea.DeliveryTag, false);
            //};

            //_channel.BasicConsume(_options.QueueNameFrom, false, consumer);

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }
    }
}
