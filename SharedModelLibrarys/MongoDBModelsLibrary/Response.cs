﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedMongoDBModelLibrary
{
    public class Response
    {
        public string Text { get; set; } = null!;
        public bool IsTrue { get; set; }
    }
}
