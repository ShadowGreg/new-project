﻿namespace CoursesAdministrator.Infrastructure.Repositories.Implementations
{
    public interface IRepository<T> : IDisposable
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T?> GetAsync(long id);
        Task<long> AddAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(long id);
    }
}
