﻿namespace Homework.DTO
{
    public class StudentMessageDTO
    {
        public string Text { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpDate { get; set; }
        public long StudentId { get; set; }

        // Сообщение учителю/учителям по курсу (CourseId)
        public string CourseId { get; set; }
        public long HomeworkId { get; set; }
    }
}
