﻿namespace ShluzAPIswagger.Models
{
    public class CourseDTO
    {
        public string? Id { get; set; }
        public string Title { get; set; } = null!;
        public string Promotion { get; set; } = null!;
        public string Description { get; set; } = null!;
        //public List<Lesson> Lessons { get; set; } = null!;

    }
}
