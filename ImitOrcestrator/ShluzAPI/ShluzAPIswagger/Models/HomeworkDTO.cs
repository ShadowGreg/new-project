﻿namespace Learn_Platform_shluz.Models
{
    public class HomeworkDTO
    {
        public long Id { get; set; }
        public long StudentId { get; set; }
        public string CourseId { get; set; }
        public string LessonId { get; set; }
        public DateTime? LastActivity { get; set; }
    }
}
