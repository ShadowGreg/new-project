﻿using ShluzAPIswagger.Models;
using SharedMongoDBModelLibrary;
using Learn_Platform_shluz.Models;

namespace ShluzAPIswagger.Services.Abstraction
{
    public interface ICourseProviderService
    {
        public List<Course> GetAll();
        public CourseProviderDTO Get(string courseId);
    }
}
