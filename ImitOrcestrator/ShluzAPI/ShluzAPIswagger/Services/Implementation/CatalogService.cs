﻿using ShluzAPIswagger.Models;
using ShluzAPIswagger.Services.Abstraction;

namespace ShluzAPIswagger.Services.Implementation
{
    public class CatalogService : ICatalogService
    {
        public List<Promotion> Get()
        {
            try
            {
                return GetPromoAsync().Result;
            } catch (Exception ex)
            { 
                Console.WriteLine(ex.Message);
                return null;
            }         
        }

        async Task<List<Promotion>> GetPromoAsync()
        {
            string url = "https://localhost:7209/api/Promotions"; // 

            List<Promotion> promotions = null;

            using (var client = new HttpClient())
            {
                //  
                var task = await client.GetFromJsonAsync(url, typeof(List<Promotion>));
                promotions = (List<Promotion>)task;
            }

            return promotions;
        }
    }
}
