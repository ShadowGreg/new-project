using Microsoft.Extensions.Configuration;
using Services.Abstractions;
using Services.Implementations;
using Microsoft.Extensions.DependencyInjection;

namespace CourseTester
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.Configure<RabbitMqOptions>(
                builder.Configuration.GetSection("RabbitMq"));


            builder.Services.AddAuthentication()
            .AddCookie(options =>
            {
                options.LoginPath = "/Account/Unauthorized/";
                options.AccessDeniedPath = "/Account/Forbidden/";
            });

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            // Read properties from json
            //var rabbitMqOptions = builder.Configuration.GetSection("RabbitMq").Get<RabbitMqOptions>();

            // Listen RabbitMq queue
            //builder.Services.AddHostedService<RabbitMqListener>(x => new RabbitMqListener(rabbitMqOptions));  // https://www.rabbitmq.com/tutorials/tutorial-six-dotnet.html
            builder.Services.AddHostedService<RabbitMqListener>();

            // RabbitMq DI
            //builder.Services.AddScoped<IRabbitMqService, RabbitMqSender>(o => new RabbitMqSender(rabbitMqOptions));
            builder.Services.AddScoped<IRabbitMqService, RabbitMqSender>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run("https://localhost:7052");
        }
    }
}