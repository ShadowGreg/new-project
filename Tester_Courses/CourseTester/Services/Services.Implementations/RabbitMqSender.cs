﻿using System.Text.Json;
using System.Text;
using Services.Abstractions;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading.Channels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Services.Implementations
{
    public class RabbitMqSender : IRabbitMqService
    {
        private readonly IRabbitMqOptions _options;

        public RabbitMqSender(IServiceProvider _serviceProvider)
        {
            using var scope = _serviceProvider.CreateScope();
            var options = scope.ServiceProvider.GetRequiredService<IOptions<RabbitMqOptions>>();

            _options = options.Value;
        }

        public void SendMessage(object obj)
        {
            var message = JsonSerializer.Serialize(obj);
            SendMessage(message);
        }

        public void SendMessage(string message)
        {
            ConnectionFactory factory;

            if (_options.Host == "localhost")
            {
                // Не забудьте вынести значения "localhost" и "MyQueue"
                // в файл конфигурации
                factory = new ConnectionFactory() { HostName = _options.Host }; // for local docker container
            }
            else
            {
                factory = new ConnectionFactory() { Uri = new Uri(_options.Host) };
            }


            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: _options.QueueNameTo,
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                    routingKey: _options.QueueNameTo,
                    basicProperties: null,
                    body: body);
            }
        }
    }
}