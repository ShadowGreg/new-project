﻿using MongoDB.Bson.IO;
using SharedMongoDBModelLibrary;
using ShluzAPIswagger.Models;
using System.Text;
using System;
using System.Text.Json;
using ShluzAPIswagger.Services.Abstraction;
using Learn_Platform_shluz.Models;
using System.Collections.Generic;
using System.Data;

namespace ShluzAPIswagger.Services.Implementation
{
    public class HomeworkService : IHomeworkService
    {
        public List<MessageDTO> AcceptHomework(int homeworkId, int mark)
        {
            try
            {
                return AcceptHomeworkAsync(homeworkId, mark).Result;
            } catch (Exception ex)
            { 
                Console.WriteLine(ex.ToString());
                return null;
            }
            
        }

        public List<MessageDTO> GetAllChat(int homeworkId, string role)
        {
            try
            {
                return GetAllChatAsync(homeworkId, role).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        public List<HomeworkDTO> GetAllHomework(string courseId)
        {
            try
            {
                return GetOpenHomeworkAsync(courseId).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        public int HomeworkMessage(string text, int userId, int homeworkId, string role)
        {
            try
            {
                return AddHomeworkMessageAsync(text, userId, homeworkId, role).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return -1;
            }
        }

        public int Submit(long studentId, string courseId, string lessonId, string text)
        {
            try
            {
                return PostHomeworkSubmitAsync(new SubmitDTO()
                {
                    StudentId = studentId,
                    CourseId = courseId,
                    LessonId = lessonId,
                    Text = text
                }).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return -1;
            }
        }

        async Task<int> PostHomeworkSubmitAsync(SubmitDTO submitEntity)
        {
            string url = "http://localhost:5206/api/Student"; // 
            var json = JsonSerializer.Serialize(submitEntity);
            int result = 0;

            using (var client = new HttpClient())
            {

                try
                {
                    using (HttpContent cont = new StringContent(json, Encoding.UTF8, "application/json"))
                    {
                        var response = await client.PostAsync(url, cont);
                        result = int.Parse(await response.Content.ReadAsStringAsync());
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Bad request!");
                }
            }

            return result;
        }

        async Task<List<HomeworkDTO>> GetOpenHomeworkAsync(string courseId)
        {
            string url = "http://localhost:5206/api/Teacher"; // 

            int result = 0;
            List<HomeworkDTO> responce = null;

            using (var client = new HttpClient())
            {
                //fill in header
                client.DefaultRequestHeaders.Add("courseId", courseId);

                responce = await client.GetFromJsonAsync<List<HomeworkDTO>>(url);

                result = 1;
            }

            return responce;
        }

        async Task<List<MessageDTO>> AcceptHomeworkAsync(int homeworkId, int mark)
        {
            string url = "http://localhost:5206/api/Teacher"; // 

            int result = 0;
            List<MessageDTO> response = null;

            using (var client = new HttpClient())
            {
                //fill in header
                client.DefaultRequestHeaders.Add("homeworkId", homeworkId.ToString());
                client.DefaultRequestHeaders.Add("value", mark.ToString());

                HttpResponseMessage resp = await client.PostAsync(url, null);

                response = await resp.Content.ReadFromJsonAsync<List<MessageDTO>>();

                result = 1;
            }

            return response;

        }

        async Task<List<MessageDTO>> GetAllChatAsync(int homeworkId, string role)
        {
            string url = string.Empty;

            if (role == "Student")
                url = "http://localhost:5206/api/Student/" + homeworkId.ToString(); //
            else
                url = "http://localhost:5206/api/Teacher/" + homeworkId.ToString(); //

            List<MessageDTO> responce = null;

            using (var client = new HttpClient())
            {
                responce = await client.GetFromJsonAsync<List<MessageDTO>>(url);
            }

            return responce;
        }

        async Task<int> AddHomeworkMessageAsync(string text, int userId, int homeworkId, string role)
        {
            int result = 0;

            string url = string.Empty;

            if(role == "Student")
                url = "http://localhost:5206/api/Student"; //
            else
                url = "http://localhost:5206/api/Teacher"; //

            MessageDTO message = new MessageDTO() { CreateDate = DateTime.Now, HomeworkId = homeworkId, Text = text, UpDate = null, UserId = userId };

            string json = JsonSerializer.Serialize(message);

            using (var client = new HttpClient())
            {
                using (HttpContent cont = new StringContent(json, Encoding.UTF8, "application/json"))
                {
                    var response = await client.PutAsync(url, cont);
                    //var userId = await response.Content.ReadAsStringAsync();

                    //return int.Parse(userId);
                    result = 1;
                }
            }

            return result;
        }
    }
}
