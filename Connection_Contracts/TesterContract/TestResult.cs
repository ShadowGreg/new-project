﻿namespace TesterContract
{
    public class TestResult
    {
       public long UserId { get; set; }
       public string CourseId { get; set; }
       public string LessonId { get; set; }
       public int PercentCorrect { get; set; } // Procent of user right answers on test
    }
}