﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Text;
using System.Threading.Channels;

namespace WebClient;

    class Program
    {
        static void Main(string[] args)
        {
            // Receive Message
            //var factory = new ConnectionFactory() { HostName = "localhost" }; // for docker container
            var factory = new ConnectionFactory() { Uri = new Uri("amqps://eexypedf:USefAchiBrLP35STdTQJ8ziZ1Qr0L86A@sparrow.rmq.cloudamqp.com/eexypedf") };
            
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {

            /*--------------------------------*/

                channel.ExchangeDeclare(exchange: "direct_admin", type: ExchangeType.Direct);

                // declare a server-named queue
                var queueName = channel.QueueDeclare().QueueName;

                channel.QueueBind(queue: queueName,
                          exchange: "direct_admin",
                          routingKey: "homework"); // severity <<<<<--------

                Console.WriteLine(" [*] Waiting for messages.");

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    var routingKey = ea.RoutingKey;
                    Console.WriteLine($" [x] Received '{routingKey}':'{message}'");
                };
                channel.BasicConsume(queue: queueName,
                                     autoAck: true,
                                     consumer: consumer);

            /*--------------------------------*/
            //channel.QueueDeclare(queue: "MyQueue",
            //                     durable: false,
            //                     exclusive: false,
            //                     autoDelete: false,
            //                     arguments: null);

            //var consumer = new EventingBasicConsumer(channel);
            //consumer.Received += (model, ea) =>
            //{
            //    var body = ea.Body.ToArray();
            //    var message = Encoding.UTF8.GetString(body);
            //    Console.WriteLine(" [x] Received {0}", message);
            //};
            //channel.BasicConsume(queue: "MyQueue",
            //                     autoAck: true,
            //                     consumer: consumer);

            Console.WriteLine(" Press [enter] to exit. Recv");
                Console.ReadLine();
            }

        //Send Message
        using (var connection = factory.CreateConnection())
        using (var channel = connection.CreateModel())
        {
            channel.QueueDeclare(queue: "HomeworkToAdminQueue",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var body = Encoding.UTF8.GetBytes("Second message for Mcs ____------>>>>>");

            channel.BasicPublish(exchange: "",
                routingKey: "HomeworkToAdminQueue",
                basicProperties: null,
                body: body);

            Console.WriteLine(" Press [enter] to exit. Send");
            Console.ReadLine();
        }
    }
    }

