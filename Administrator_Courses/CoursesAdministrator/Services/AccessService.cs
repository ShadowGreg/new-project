﻿using CoursesAdministrator.Infrastructure.Repositories.Implementations;
using CoursesAdministrator.Models;
using PostgreSQLModelsLibrary;

namespace CoursesAdministrator.Services
{
    public class AccessService : IAccessService
    {
        private readonly IRepository<Access> _accessRepository;
        private readonly IAuthorizerConnector _authorizerConnector;
        private readonly IProgressService _progressService;
        private const string TEACHER = "Teacher";
        private const string STUDENT = "Student";

        public AccessService(IRepository<Access> accessRepository, IAuthorizerConnector authorizerConnector, IProgressService progressService)
        {
            _accessRepository = accessRepository;
            _authorizerConnector = authorizerConnector;
            _progressService = progressService;
        }

        public async Task RequestAccess(AccessModel model)
        {
            var updateAccess = (await _accessRepository.GetAllAsync()).FirstOrDefault(a => a.UserId == model.UserId && a.CourseId.Equals(model.CourseId));

            if (updateAccess != null) //запрос доступа уже был
            {
                return;
            }

            var role = await _authorizerConnector.GetRole(model.UserId);

            var newAccess = new Access()
            {
                UserId = model.UserId,
                CourseId = model.CourseId,
                Read = false,
                Write = false,
                Delete = false
            };

            if (role.Contains(TEACHER)) 
            {
                newAccess.Read= true;
            }

            await _accessRepository.AddAsync(newAccess);
        }

        public async Task<Access?> GetAccessByCourse(long userId, string courseId)
        {
            return (await _accessRepository.GetAllAsync()).FirstOrDefault(a => a.UserId == userId && a.CourseId.Equals(courseId));
        }

        public async Task GiveAccess(AccessModel model)
        {
            var role = await _authorizerConnector.GetRole(model.UserId);
            var updateAccess = (await _accessRepository.GetAllAsync()).FirstOrDefault(a => a.UserId == model.UserId && a.CourseId.Equals(model.CourseId));
            
            if (updateAccess is null)
            {
                return;
            }

            updateAccess.Read = true;

            if (role.Contains(TEACHER))
            {
                updateAccess.Write = true;
            }

            if (role.Contains(STUDENT))
            {
                await _progressService.AddProgress(new ProgressModel() { 
                    CourseId= model.CourseId, 
                    LessonId = model.LessonId, 
                    UserId = model.UserId 
                });
            }

            await _accessRepository.UpdateAsync(updateAccess);
        }

        public async Task RevertAccess(AccessModel model)
        {
            var role = await _authorizerConnector.GetRole(model.UserId);
            var updateAccess = (await _accessRepository.GetAllAsync()).FirstOrDefault(a => a.UserId == model.UserId && a.CourseId.Equals(model.CourseId));
            
            if (updateAccess is null)
            {
                return;
            }
            updateAccess.Write = false;

            if (role.Contains(TEACHER))
            {
                updateAccess.Read = false;
            }

            await _accessRepository.UpdateAsync(updateAccess);
        }

        public async Task<IEnumerable<Access?>> GetAccess(long userId)
        {
            return (await _accessRepository.GetAllAsync()).Where(a => a.UserId == userId);
        }

        public async Task<IEnumerable<Access?>> GetRequests()
        {
            var accessList = await _accessRepository.GetAllAsync();
            return accessList.Where(access =>
            {
                var role = _authorizerConnector.GetRole(access.UserId).GetAwaiter().GetResult();
                return role.Contains(TEACHER) && !access.Write || !role.Contains(TEACHER) && !access.Read;
            });
        }

        public async Task<IEnumerable<long>> GetTeachersByCourse(string courseId)
        {
            var accessList = await _accessRepository.GetAllAsync();
            return accessList.Where(access => access.CourseId == courseId && access.Write).Select(a => a.UserId);
        }
    }
}
