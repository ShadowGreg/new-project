﻿using MongoDB.Driver;

namespace ProviderCourses.DataAccess
{
    public interface IDBMongoConnectGet
    {
        IMongoCollection<CourseDTO> GetConnect();
    }
}
