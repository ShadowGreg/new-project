﻿using Microsoft.AspNetCore.Mvc;
using PostgreSQLModelsLibrary;
using ShluzAPIswagger.Models;

namespace ShluzAPIswagger.Services.Abstraction
{
    public interface IAdministratorService
    {
        public List<Access> Get(int userId);

        public int RequestAccess(int userId, string courseId, string lessonId);

        public List<Access> GetAccessRequests();

        public int GiveAccess(int userId, string courseId, string lessonId);
        public int RevertAccess(int userId, string courseId);

        public List<Progress> GetProgress(int userId, string courseId);
    }
}
