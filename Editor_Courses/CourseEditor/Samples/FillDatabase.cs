﻿using CourseEditor.DataAccess;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using SharedMongoDBModelLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseEditor.Samples
{
    public static class FillDatabase
    {


        public static SharedMongoDBModelLibrary.Course Course1()
        {
            return new SharedMongoDBModelLibrary.Course
            {
                Promotion = "Основные особенности языка программирования Python:\r\n\r\nСкриптовый язык. Код программ определяется в виде скриптов.\r\n\r\nПоддержка самых различных парадигм программирования, в том числе объектно-ориентированной и функциональной парадигм.\r\n\r\nИнтерпретация программ. Для работы со скриптами необходим интерпретатор, который запускает и выполняет скрипт.\r\n\r\nВыполнение программы на Python выглядит следующим образом. Сначала мы пишим в текстовом редакторе скрипт с набором выражений на данном языке программирования. Передаем этот скрипт на выполнение интерпретатору. Интерпретатор транслирует код в промежуточный байткод, а затем виртуальная машина переводит полученный байткод в набор инструкций, которые выполняются операционной системой.",
                Description = "Python представляет популярный высокоуровневый язык программирования, который предназначен для создания приложений различных типов. Это и веб-приложения, и игры, и настольные программы, и работа с базами данных. Довольно большое распространение питон получил в области машинного обучения и исследований искусственного интеллекта.\r\n\r\nВпервые язык Python был анонсирован в 1991 году голландским разработчиком Гвидо Ван Россумом. С тех пор данный язык проделал большой путь развития. В 2000 году была издана версия 2.0, а в 2008 году - версия 3.0. Несмотря на вроде такие большие промежутки между версиями постоянно выходят подверсии. Так, текущей актуальной версией на момент написания данного материала является 3.11, которая вышла в октябре 2022 года.",
                Title = "Руководство по языку программирования Python",
                IsRemoved = false,
                Lessons = new List<SharedMongoDBModelLibrary.Lesson> {
                    new SharedMongoDBModelLibrary.Lesson {
                        Id=ObjectId.GenerateNewId().ToString(),
                        Title="Введение в написание программ",
                        Description="Программа на языке Python состоит из набора инструкций. Каждая инструкция помещается на новую строку. Например:\r\n\r\n1\r\n2\r\nprint(2 + 3) \r\nprint(\"Hello\")\r\nБольшую роль в Python играют отступы. Неправильно поставленный отступ фактически является ошибкой. Например, в следующем случае мы получим ошибку, хотя код будет практически аналогичен приведенному выше:\r\n\r\n1\r\n2\r\nprint(2 + 3) \r\n    print(\"Hello\")\r\nПоэтому стоит помещать новые инструкции сначала строки.\r\n ...",
                        Links=new List<string>{ "https://metanit.com/python/tutorial/2.1.php"},
                        IsRemoved = false,        
                    },
                    new SharedMongoDBModelLibrary.Lesson
                    {
                        Id=ObjectId.GenerateNewId().ToString(),
                        Title ="Переменные и типы данных",
                        Description="Переменные\r\nПеременные предназначены для хранения данных. Название переменной в Python должно начинаться с алфавитного символа или со знака подчеркивания и может содержать алфавитно-цифровые символы и знак подчеркивания. И кроме того, название переменной не должно совпадать с названием ключевых слов языка Python. Ключевых слов не так много, их легко запомнить:\r\n...",
                        Links=new List<string>{ "https://metanit.com/python/tutorial/2.2.php"},
                        Homework="Присвогить переменным разных типов данных значения",
                        IsRemoved = false        
                    },
                    new SharedMongoDBModelLibrary.Lesson {
                            Id=ObjectId.GenerateNewId().ToString(),
                            Title="в уроке только тест ",
                            Description="пройдите тест",
                            IsRemoved = false,
                            Test=new List<Question>
                            {
                                new Question
                                {
                                    Text="выберите правильные ответы",
                                    Responses=new List<Response>
                                    {
                                        new Response{Text="Название переменной в Python должно начинаться с алфавитного символа ",IsTrue=true},
                                        new Response{Text="Каждая инструкция помещается на новую строку",IsTrue=true},
                                        new Response{Text="Python - старейший ЯП",IsTrue=false},
                                    }
                                }
                            }
                    }

                }

            };

        }
        public static SharedMongoDBModelLibrary.Course Course2()
        {
            return new SharedMongoDBModelLibrary.Course
            {
                Promotion = "Сегодняшний мир веб-сайтов трудно представить без языка JavaScript. JavaScript - это то, что делает живыми веб-страницы, которые мы каждый день просматриваем в своем веб-браузере.",
                Description = "Сегодняшний мир веб-сайтов трудно представить без языка JavaScript. JavaScript - это то, что делает живыми веб-страницы, которые мы каждый день просматриваем в своем веб-браузере.\r\n\r\nJavaScript был создан в 1995 году в компании Netscape разработчиком Брендоном Айком (Brendon Eich) в качестве языка сценариев в браузере Netscape Navigator 2. Первоначально язык назывался LiveScript, но на волне популярности в тот момент другого языка Java LiveScript был переименован в JavaScript. ",
                Title = " Руководство по JavaScript",
                Lessons = new List<SharedMongoDBModelLibrary.Lesson> {
                    new SharedMongoDBModelLibrary.Lesson {
                            Id=ObjectId.GenerateNewId().ToString(),
                            Title="первый урок",
                            Description="пройдите тест",
                            IsRemoved = false,
                            Test=new List<Question>
                            {
                                new Question
                                {
                                    Text="выберите 2й пункт",
                                    Responses=new List<Response>
                                    {
                                        new Response{Text="пункт1",IsTrue=false},
                                        new Response{Text="пункт2",IsTrue=true},
                                        new Response{Text="пункт3",IsTrue=false},
                                    }
                                }
                            }
                    }
                },
                IsRemoved = false
        };
        }

    }
}
