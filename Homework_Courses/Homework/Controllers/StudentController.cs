﻿using Microsoft.AspNetCore.Mvc;
using Homework.DTO;
using Homework.DataAccess;
using Homework.Services;

namespace Homework.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IHomeworkRepo _repo;
        private readonly IRabbit _mq;

        public StudentController(IHomeworkRepo repo, IRabbit mq)
        {
            _repo = repo;
            _mq = mq;
        }

        // GET: api/<StudentController>
        /// <summary>
        /// Открытые домашки студента
        /// </summary>
        /// <param name="StudentId"></param>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<HomeworkDTO> GetOpen([FromHeader]  long StudentId) // смотрим все ДЗ на студента
        {
            return _repo.GetOpen(StudentId);
        }

        // GET api/<StudentController>/5
        /// <summary>
        /// Переписка по ДЗ
        /// </summary>
        /// <param name="homeworkId"></param>
        /// <returns></returns>
        [HttpGet("{homeworkId}")]
        public IEnumerable<MessageDTO> Get(long homeworkId) // смотрим все сообщения (чат) связанные с конкретным ДЗ 
        {
            return _repo.GetMessages(homeworkId); // достаем из таблицы Messages все сообщения для конкретного ДЗ
        }

        // POST api/<StudentController>

        /// <summary>
        /// Сдать домашнее задание
        /// </summary>
        /// <param name="work"></param>
        /// <returns></returns>
        [HttpPost]        
        public long SubmitHomework(SubmitDTO work) // сдаем дз на проверку (заполняем данные о ДЗ и пишем сообщение)
        {
            var res = _repo.Submit(work).Result;

            _mq.SendMessage(new { 
                CourseId = work.CourseId,
                StudentId = work.StudentId,
                LessonId = work.LessonId,
                Text = work.Text,
                HomeworkId = res }, "submit"); // signal to Email_Notifier about submit homework
           
            return res;
        }

        // PUT api/<StudentController>/5
        /// <summary>
        /// Добавить сообщение в чат с преподавателем
        /// </summary>
        /// <param name="msg"></param>
        [HttpPut]
        public void AddMessage( [FromBody] MessageDTO msg) // добавляем сообщение к переписке конкретного ДЗ
        {
            _repo.AddMesage(msg);

            StudentMessageDTO studentMessage = new StudentMessageDTO()
            {
                Text = msg.Text,
                StudentId = msg.UserId,
                CourseId = _repo.GetHomework(msg.HomeworkId).CourseId,
                CreateDate = msg.CreateDate,
                UpDate = msg.UpDate,
                HomeworkId = msg.HomeworkId
            };

            _mq.SendMessage(studentMessage, "studentmessage"); // signal to Email_Notifier about new message
        }



      
    }
}
