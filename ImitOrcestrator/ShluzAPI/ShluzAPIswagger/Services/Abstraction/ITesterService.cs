﻿using SharedMongoDBModelLibrary;
using ShluzAPIswagger.Models;

namespace ShluzAPIswagger.Services.Abstraction
{
    public interface ITesterService
    {
        List<Question> GetTest(int userId, string courseId, string lessonId);

        int SendAnswers(int userId, string courseId, string lessonId, List<Question> test);
    }
}
