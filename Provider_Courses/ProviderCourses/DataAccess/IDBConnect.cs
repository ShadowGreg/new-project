﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
namespace ProviderCourses.DataAccess
{
    public interface IDBConnect
    {
      void Connect(IOptions<CourseStoreDatabaseSettings> courceStoreDatabaseSettings);
      //IMongoCollection<CourseDTO> GetConnect();

    }
}
