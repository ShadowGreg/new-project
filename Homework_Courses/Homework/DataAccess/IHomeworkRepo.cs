﻿using Homework.DTO;

namespace Homework.DataAccess
{
    public interface IHomeworkRepo
    {
        IEnumerable<HomeworkDTO> GetOpen(long UserId);
        IEnumerable<HomeworkDTO> GetOpen(string CourseId);
        Task<long> Submit(SubmitDTO work);
        Task AddMesage(MessageDTO msg);
        IEnumerable<MessageDTO> GetMessages(long homeworkId);
        HomeworkDTO GetHomework(long homeworkId);
        AcceptMessage Accept(int homeworkId, int value);
    }
}
