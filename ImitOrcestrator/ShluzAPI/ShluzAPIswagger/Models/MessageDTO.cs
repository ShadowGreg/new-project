﻿namespace Learn_Platform_shluz.Models
{
    public class MessageDTO
    {
        public string Text { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpDate { get; set; }
        public long UserId { get; set; }

        // Связь с таблицей Homework
        public long HomeworkId { get; set; }
    }
}
