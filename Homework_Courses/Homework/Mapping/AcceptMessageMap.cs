﻿using AutoMapper;

namespace Homework.Mapping
{
    public class AcceptMessageMap : Profile
    {
        public AcceptMessageMap()
        {
            CreateMap<DTO.HomeworkDTO, DTO.AcceptMessage>()
               .ForMember(dest => dest.UserId, o => o.MapFrom(src => src.StudentId))
               .ForMember(dest => dest.PercentCorrect, o => o.MapFrom(src => 0))
               ;
        }
    }
}
