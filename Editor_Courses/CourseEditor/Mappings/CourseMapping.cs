﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace CourseEditor.Mappings
{
    public class CourseMapping:Profile
    {
        public CourseMapping()
        {
            CreateMap<SharedMongoDBModelLibrary.Course,CourseEditorContract.Course >().ReverseMap()
                .ForMember(dest =>dest.DateUpdated,opt=>opt.MapFrom(src=>DateTime.Now))   
                .ForMember(dest =>dest.DateCreated,opt=>opt.MapFrom(src=> DateTime.Now))   
                .ForMember(dest =>dest.IsRemoved,opt=>opt.MapFrom(src=>false))   
                .ForMember(dest =>dest.Lessons,opt=>opt.MapFrom(src=>src.Lessons))   
                ;
            CreateMap<SharedMongoDBModelLibrary.Course, CourseEditorContract.CourseHdr>().ReverseMap()
                .ForMember(dest => dest.DateUpdated, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.DateCreated, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.IsRemoved, opt => opt.MapFrom(src => false))
                .ForMember(dest => dest.Lessons, opt => opt.MapFrom(src => new List<SharedMongoDBModelLibrary.Lesson>()))
                ;
            CreateMap<SharedMongoDBModelLibrary.Course, CourseEditorContract.CourseNew>().ReverseMap()
                .ForMember(dest => dest.DateUpdated, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.DateCreated, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.IsRemoved, opt => opt.MapFrom(src => false))
                .ForMember(dest => dest.Lessons, opt => opt.MapFrom(src => new List<SharedMongoDBModelLibrary.Lesson>()))
                ;
            CreateMap<SharedMongoDBModelLibrary.Lesson, CourseEditorContract.Lesson>().ReverseMap()
                .ForMember(dest => dest.DateUpdated, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.DateCreated, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.IsRemoved, opt => opt.MapFrom(src => false))
                .ForMember(dest => dest.Test, opt => opt.MapFrom(src => src.Test))
                ;

            CreateMap<SharedMongoDBModelLibrary.Question, CourseEditorContract.Question>().ReverseMap();

            CreateMap<SharedMongoDBModelLibrary.Response, CourseEditorContract.Response>().ReverseMap();
        }
    }
}
