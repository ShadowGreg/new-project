﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace ProviderCourses.DataAccess
{
    public class DBMongoConnect : IDBConnect, IDBMongoConnectGet
    {
        private IMongoCollection<CourseDTO> _coursesCollection;
        public void Connect(IOptions<CourseStoreDatabaseSettings> courceStoreDatabaseSettings1)
        {
 
            IOptions<CourseStoreDatabaseSettings> courceStoreDatabaseSettings = courceStoreDatabaseSettings1;
            var mongoClient = new MongoClient(courceStoreDatabaseSettings.Value.ConnectionString);
            var mongoDatabase = mongoClient.GetDatabase(courceStoreDatabaseSettings.Value.DatabaseName);
            _coursesCollection = mongoDatabase.GetCollection<CourseDTO>(courceStoreDatabaseSettings.Value.BooksCollectionName);
        }
        public IMongoCollection<CourseDTO> GetConnect()
        {
            return _coursesCollection;
        }
    }
}
