﻿namespace ShluzAPIswagger.Models
{
    public class AccessRequestDTO
    {
        public long UserId { get; set; }
        public string CourseId { get; set; }
        public string LessonId { get; set; }
    }
}
