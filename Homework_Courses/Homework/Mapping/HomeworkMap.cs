﻿using AutoMapper;
using System;

namespace Homework.Mapping
{
    public class HomeworkMap : Profile
    {
        public HomeworkMap()
        {
            CreateMap< Entity.Homework, DTO.HomeworkDTO>().ReverseMap()
                .ForMember(dest=>dest.UpDate,o=>o.MapFrom(src=>src.LastActivity))                
                .ForMember(dest=>dest.Deleted,o=>o.MapFrom(src=>false))
                ;

            CreateMap<Entity.Homework, DTO.HomeworkDTO>()
                .ForMember(dest => dest.LastActivity, o => o.MapFrom(src => src.UpDate))
                ;
        }

    }
    
}
