﻿using CoursesAdministrator.Infrastructure.Repositories.Implementations;
using CoursesAdministrator.Models;
using CoursesAdministrator.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoursesAdministrator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccessController : ControllerBase
    {

        private readonly IAccessService _accessService;

        public AccessController(IAccessService accessService)
        {
            _accessService = accessService;
        }

        [HttpGet("{userId}/course/{courseId}")] 
        public async Task<IActionResult> GetAccessAsync(long userId, string courseId)
        {
            return Ok(await _accessService.GetAccessByCourse(userId, courseId));
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetAccessAsync(long userId)
        {
            return Ok(await _accessService.GetAccess(userId));
        }

        [HttpPost("request")]
        public async Task<IActionResult> RequestAccessAsync([FromBody] AccessModel access) // запрос на доступ к курсу
        {
            await _accessService.RequestAccess(access);
            return Ok();         
        }

        [HttpPost("give")]
        public async Task<IActionResult> GiveAccessAsync([FromBody] AccessModel access)
        {
            await _accessService.GiveAccess(access);
            return Ok();
        }

        [HttpPost("revert")]
        public async Task<IActionResult> RevertAccessAsync([FromBody] AccessModel access)
        {
            await _accessService.RevertAccess(access);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetRequestsAsync()
        {
            return Ok(await _accessService.GetRequests());
        }

        [HttpGet("teachers/{courseId}")]
        public async Task<IActionResult> GetTeachersByCourseAsync(string courseId)
        {
            return Ok(await _accessService.GetTeachersByCourse(courseId));
        }
    }
}
