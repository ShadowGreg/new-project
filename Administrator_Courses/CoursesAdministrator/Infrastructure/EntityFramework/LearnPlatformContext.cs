﻿using Microsoft.EntityFrameworkCore;
using PostgreSQLModelsLibrary;

namespace CoursesAdministrator.Infrastructure.EntityFramework
{
    public class LearnPlatformContext : DbContext
    {
        public DbSet<Progress> Progresses { get; set; }
        public DbSet<Access> Accesses { get; set; }
        public LearnPlatformContext()
        {
            Database.EnsureCreated();
        }
        public LearnPlatformContext(DbContextOptions<LearnPlatformContext> options) : base(options)
        {

        }
    }
}
