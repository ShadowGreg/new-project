﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SharedMongoDBModelLibrary;
using СourseСatalog.Domain;
using СourseСatalog.Services;
using СourseСatalog.WebApi.Models;

namespace СourseСatalog.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromotionsController : ControllerBase
    {
        private readonly PromotionServiceCached _promotionSevice;

        public PromotionsController(PromotionServiceCached promotionService)
        {
            _promotionSevice = promotionService;
        }

        [HttpGet]
        public async Task<List<Promotion>> Get() => await _promotionSevice.GetPromotions();
    }
}
