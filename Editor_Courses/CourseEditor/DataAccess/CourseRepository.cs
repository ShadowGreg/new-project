﻿using CourseEditor.Model;
using CourseEditor.Samples;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using SharedMongoDBModelLibrary;

namespace CourseEditor.DataAccess
{
    public class CourseRepository : ICourseRepository
    {
        private readonly IMongoCollection<Course> _coursesCollection;

        public CourseRepository(
            IOptions<CourseStoreDatabaseSettings> courceStoreDatabaseSettings)
        {
            var mongoClient = new MongoClient(
                courceStoreDatabaseSettings.Value.ConnectionString);

            var mongoDatabase = mongoClient.GetDatabase(courceStoreDatabaseSettings.Value.DatabaseName);

            _coursesCollection = mongoDatabase.GetCollection<Course>(courceStoreDatabaseSettings.Value.BooksCollectionName);
        }

        public async Task<DBResponse> MakeSamples()
        {
            if (_coursesCollection.CountDocuments(new BsonDocument()) == 0)
            {
                _coursesCollection.InsertOneAsync(FillDatabase.Course1());
                _coursesCollection.InsertOneAsync(FillDatabase.Course2());
                return new DBResponse { Code = Codes.Ok };
            }
            else 
            { 
                return new DBResponse { Code = Codes.Fail, ResultText = "CoursesDB is not empty" }; 
            }
        }

        public async Task<DBResponse> NewCourse(string title, string promotion, string description)
        {
            try
            {

                var course = new SharedMongoDBModelLibrary.Course
                {
                    Title = title,
                    Promotion= promotion,
                    Description = description,
                };


                if (_coursesCollection.FindAsync(c => c.Title == course.Title).Result.ToList().Count > 0)
                {
                    return new DBResponse { Code = Codes.Fail, ResultText = "Course with same Title already exist" };
                }

                course.DateCreated = DateTime.Now;
                course.IsRemoved = false;
                if (course.Lessons is null)
                {
                    course.Lessons = new List<Lesson>();
                }
                await _coursesCollection.InsertOneAsync(course);
                return new DBResponse { Code = Codes.Ok, Id = course.Id.ToString() };
            }
            catch (Exception e)
            {
                return new DBResponse { Code = Codes.Fail, ResultText=e.ToString() };
            }
        }

        public async Task<DBResponse> EditCourse(Course course)
        {
            try 
            { 

                if (_coursesCollection.FindAsync(c => c.Id == course.Id).Result.ToList().Count == 0)
                {
                    return new DBResponse { Code = Codes.Fail, ResultText = "Course not found" };
                }

                var o= _coursesCollection.Find(c => c.Id == course.Id).First();

                course.Lessons = o.Lessons;

                await _coursesCollection.FindOneAndReplaceAsync(c => c.Id == course.Id, course);
                return new DBResponse { Code = Codes.Ok, Id = course.Id.ToString() };
            }
            catch (Exception e)
            {
                return new DBResponse { Code = Codes.Fail, ResultText=e.ToString()    };
            }
        }

        public async Task<DBResponse> DelCourse(string id)
        {
            try
            {

                var record = await _coursesCollection.FindOneAndUpdateAsync(
                    Builders<Course>.Filter.Where(rec => rec.Id == id),
                    Builders<Course>.Update.Set(rec => rec.IsRemoved, true));

                return new DBResponse { Code = record != null ? Codes.Ok : Codes.Fail, ResultText = $"deleted {record.Id}", Id = record.Id };
            }
             
            catch (Exception e)
            {
                return new DBResponse { Code = Codes.Fail, ResultText=e.ToString()    };
            }
        }


        public async Task<Course> GetAsync(string id) =>
            await _coursesCollection.FindAsync(x => x.Id == id).Result.FirstAsync();

        public async Task<List<Course>> GetAsync() =>
            await _coursesCollection.Find(x=>x.IsRemoved==false).ToListAsync();

        public void DropDatabase()
        {
            _coursesCollection.DeleteMany(_ => true);
        }
    }
}
