﻿namespace CourseProviderContract
{
    public class Question
    {
        public string ?Text { get; set; }   // 

        public List<Response> ?Responses { get; set; }  //
    }
}