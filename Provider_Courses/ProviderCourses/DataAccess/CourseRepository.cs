﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Bson;

namespace ProviderCourses.DataAccess
{
    public class CourseRepository : ICourseRepository
    {
        private readonly IMongoCollection<CourseDTO> _coursesCollection;

        public CourseRepository(
            IOptions<CourseStoreDatabaseSettings> courceStoreDatabaseSettings, IDBConnect con, IDBMongoConnectGet con2)
        {
            con.Connect(courceStoreDatabaseSettings);
            _coursesCollection = con2.GetConnect();
        }
   
        public async Task<CourseDTO> GetAsync(string id) =>
            await _coursesCollection.FindAsync(x => x.Id == id).Result.FirstAsync();

        public async Task<List<CourseDTO>> GetAsync()
        {
            var result = await _coursesCollection.Find(_ => true).ToListAsync();

            return result;
        } 
    }
}
