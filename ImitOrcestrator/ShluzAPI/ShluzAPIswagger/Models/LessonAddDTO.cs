﻿using SharedMongoDBModelLibrary;

namespace ShluzAPIswagger.Models
{
    public class LessonAddDTO
    {
        public string? Id { get; set; }
        public string Title { get; set; } = null!;
        public string Description { get; set; } = null!;
        public string Homework { get; set; } = null!;
        public List<Question> Test { get; set; } = null!;
        public List<string> Links { get; set; } = null!;
    }
}
