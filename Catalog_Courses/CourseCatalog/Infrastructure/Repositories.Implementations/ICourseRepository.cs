﻿using SharedMongoDBModelLibrary;

namespace СourseСatalog.Infrastructure.Repositories.Implementations
{
    public interface ICourseRepository
    {
        Task<List<Course>> GetAsync();
    }
}
