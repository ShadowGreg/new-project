﻿namespace ShluzAPIswagger.Models
{
    public class SubmitDTO
    {
        public long StudentId { get; set; }
        public string CourseId { get; set; }
        public string LessonId { get; set; }
        public string Text { get; set; }
    }
}
