﻿using RabbitMQ.Client;
using System.Text.Json;
using System.Text;
using Microsoft.Extensions.Options;

namespace CoursesAdministrator.RabbitMQ
{
    public class RabbitMqService : IRabbitMqService
    {
        private readonly RabbitMQSettings _rabbitMQSettings;

        public RabbitMqService(IOptions<RabbitMQSettings> rabbitMqSettings)
        {
            _rabbitMQSettings = rabbitMqSettings.Value;
        }
        public void SendMessage(object obj, string severity)
        {
            var message = JsonSerializer.Serialize(obj);
            SendMessage(message, severity);
        }

        private void SendMessage(string message, string severity)
        {
            ConnectionFactory factory;
            //var factory = new ConnectionFactory() { Uri = new Uri(_rabbitMQSettings.Url) };

            if (_rabbitMQSettings.Url == "localhost")
            {
                factory = new ConnectionFactory() { HostName = _rabbitMQSettings.Url }; // for local docker container
            }
            else
            {
                factory = new ConnectionFactory() { Uri = new Uri(_rabbitMQSettings.Url) };
            }

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: _rabbitMQSettings.Direct, type: ExchangeType.Direct); ;

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: _rabbitMQSettings.Direct,
                     routingKey: severity,
                     basicProperties: null,
                     body: body);

                Console.WriteLine($" [x] Sent '{severity}':'{message}'");
            }
        }
    }
}
