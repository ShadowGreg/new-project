﻿using CourseEditor.Model;
using SharedMongoDBModelLibrary;

namespace CourseEditor.DataAccess
{
    public interface ICourseRepository
    {
        Task<DBResponse> NewCourse(string title, string promotion, string description);        
        Task<List<Course>> GetAsync();
        Task<Course> GetAsync(string id);
        Task<DBResponse> DelCourse(string id);
        Task<DBResponse> EditCourse(Course course);

        Task<DBResponse> MakeSamples();

        void DropDatabase();
    }


}
