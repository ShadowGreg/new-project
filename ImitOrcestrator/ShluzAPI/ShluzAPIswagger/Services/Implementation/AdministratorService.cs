﻿using ShluzAPIswagger.Models;
using PostgreSQLModelsLibrary;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Text;
using ShluzAPIswagger.Services.Abstraction;
using SharedMongoDBModelLibrary;

namespace ShluzAPIswagger.Services.Implementation
{
    public class AdministratorService : IAdministratorService
    {
        public List<Progress> GetProgress(int userId, string courseId)
        {
            return GetProgressAsync(userId, courseId).Result;
        }

        public List<Access> Get(int userId)
        {
            try
            {
                return GetAccessAsync(userId).Result;
            } 
            catch (Exception ex)
            { 
                Console.WriteLine(ex.Message);
                return null;
            }           
        }

        public List<Access> GetAccessRequests()
        {         
            try
            {
                return GetAccessRequestsAsync().Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public int RequestAccess(int userId, string courseId, string lessonId)
        {
            try
            {
                return RequestAccessAsync(new AccessRequestDTO() { UserId = userId, CourseId = courseId, LessonId = lessonId }).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }          
        }

        public int GiveAccess(int userId, string courseId, string lessonId)
        {
            try
            {

                return GiveAccessAsync(new AccessRequestDTO() { UserId = userId, CourseId = courseId, LessonId = lessonId }).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }

        }

        public int RevertAccess(int userId, string courseId)
        {
            
            try
            {

                return RevertAccessAsync(new AccessRequestDTO() { UserId = userId, CourseId = courseId }).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
        }

        async Task<List<Access>> GetAccessAsync(int userId)
        {
            string url = "https://localhost:7239/api/Access/" + userId.ToString(); // 

            List<Access> dostups = null;

            using (var client = new HttpClient())
            {

                var task = await client.GetFromJsonAsync(url, typeof(List<Access>));
                dostups = (List<Access>)task;
            }

            return dostups;
        }

        async Task<int> RequestAccessAsync(AccessRequestDTO accessRequest)
        {
            string url = "https://localhost:7239/api/Access/request"; // 

            var json = JsonSerializer.Serialize(accessRequest);
            int result = 0;

            using (var client = new HttpClient())
            {

                try
                {
                    using (HttpContent cont = new StringContent(json, Encoding.UTF8, "application/json"))
                    {
                        var response = await client.PostAsync(url, cont);
                        //result = Int32.Parse(await response.Content.ReadAsStringAsync());
                        result = 1;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Bad request to https://localhost:7239/api/Access/request!");
                    return -1;
                }
            }

            return result;
        }

        async Task<List<Access>> GetAccessRequestsAsync()
        {
            string url = "https://localhost:7239/api/Access"; // 

            List<Access> dostups = null;

            using (var client = new HttpClient())
            {

                var task = await client.GetFromJsonAsync(url, typeof(List<Access>));
                dostups = (List<Access>)task;
            }

            return dostups;
        }

        async Task<int> GiveAccessAsync(AccessRequestDTO accessRequest)
        {
            string url = "https://localhost:7239/api/Access/give"; // 

            var json = JsonSerializer.Serialize(accessRequest);
            int result = 0;

            using (var client = new HttpClient())
            {

                try
                {
                    using (HttpContent cont = new StringContent(json, Encoding.UTF8, "application/json"))
                    {
                        var response = await client.PostAsync(url, cont);
                        //result = Int32.Parse(await response.Content.ReadAsStringAsync());
                        result = 1;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Bad request to https://localhost:7239/api/Access/give!");
                    return -1;
                }
            }

            return result;
        }

        async Task<int> RevertAccessAsync(AccessRequestDTO accessRequest)
        {
            string url = "https://localhost:7239/api/Access/revert"; // 

            var json = JsonSerializer.Serialize(accessRequest);
            int result = 0;

            using (var client = new HttpClient())
            {

                try
                {
                    using (HttpContent cont = new StringContent(json, Encoding.UTF8, "application/json"))
                    {
                        var response = await client.PostAsync(url, cont);
                        //result = Int32.Parse(await response.Content.ReadAsStringAsync());
                        result = 1;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Bad request to https://localhost:7239/api/Access/give!");
                    return -1;
                }
            }

            return result;
        }

        async Task<List<Progress>> GetProgressAsync(int userId, string courseId)
        {
            string url = "https://localhost:7239/api/Progress/" + userId.ToString() + "/course/" + courseId;

            List<Progress> progresses = null;

            using (var client = new HttpClient())
            {

                var task = await client.GetFromJsonAsync(url, typeof(List<Progress>));
                progresses = (List<Progress>)task;
            }

            return progresses;
        }
    }
}
