﻿using System.Text.Json;

namespace CoursesAdministrator.Services
{
    public class AuthorizerConnector : IAuthorizerConnector
    {
        public async Task<List<string>> GetRole(long userId)
        {
            var client = new HttpClient();
            using var response = await client.GetAsync($"https://localhost:7029/api/UserRole/{userId}");
            var rolesJson = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<List<string>>(rolesJson);
        }
    }
}
