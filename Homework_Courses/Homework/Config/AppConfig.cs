﻿namespace Homework.Config
{
    public class AppConfig
    {
        public string DBconnection { get; set; }
        public string MQconnection { get; set; }
        public string MQqueue { get; set; }
    }
}
