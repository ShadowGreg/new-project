﻿using Services.Abstractions;

namespace Services.Implementations
{
    public class RabbitMqOptions : IRabbitMqOptions
    {
      public string? QueueNameTo { get; set; }
      public string? QueueNameFrom { get; set; }  
      public string? Host { get; set; }

    }
}
